import threading
import pexpect
import struct
import time
import sys
import numpy as np
import math
import os

import matplotlib.pyplot as plt
import matplotlib.animation as animation

from scipy.interpolate import interp1d
from sklearn.preprocessing import scale

TYPE_ACCELEROMETER = 0
TYPE_GYROSCOPE = 1
TYPE_MAGNETIC_FIELD = 2

BT_MAC_ADDRESS="DE:B4:F8:AA:BC:43"
#BT_MAC_ADDRESS="D7:A5:66:A1:4C:9E"
UUID_DATA_TX="6e400002-b5a3-f393-e0a9-e50e24dcca9e"
UUID_DATA_RX="6e400003-b5a3-f393-e0a9-e50e24dcca9e"
UUID_DATA_TX_HDR="0x0010"
UUID_DATA_RX_HDR="0x000d"
UUID_DATA_RX_NF_HDR="0x000e"
DATA_PACK=0
DATA_LENGTH=0
STATE=0 #0->wait for data notification, 1->wait for data
frame=[]
accDataLock = threading.RLock()
accBufLen=1000

class sensorFusion:
    gyro = [0.0, 0.0, 0.0]
    gyroMatrix = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
    gyroOrientation = [0.0, 0.0, 0.0]
    magnet = [0.0, 0.0, 0.0]
    accel = [0.0, 0.0, 0.0]
    accMagOrientation = [0.0, 0.0, 0.0]
    fusedOrientation = [0.0, 0.0, 0.0]
    rotationMatrix = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    FILTER_COEFFICIENT = 0.98
    EPSILON = 0.000000001

    accMagOrientationValid = 0

    initState = True
    timestamp = 0

    def __init__(self):
        self.timestamp = 0
        return
    def getRotationMatrix(self, R, I, gravity, geomagnetic):
        Ax = gravity[0]
        Ay = gravity[1]
        Az = gravity[2]

        normsqA = Ax * Ax + Ay * Ay + Az * Az
        #here we use 4G accel on the sensor device
        g = 4.0
        freeFallGravitySquared = 0.01 * g * g
        if normsqA < freeFallGravitySquared:
            return False

        Ex = geomagnetic[0]
        Ey = geomagnetic[1]
        Ez = geomagnetic[2]

        Hx = Ey * Az - Ez * Ay
        Hy = Ez * Ax - Ex * Az
        Hz = Ex * Ay - Ey * Ax
        #print "Hx:%.4f, Hy:%.4f, Hz:%.4f"%(Hx, Hy, Hz)
        normH = np.sqrt(Hx * Hx + Hy * Hy + Hz * Hz)
        if (normH < 0.1):
            return False

        invH = 1.0 / normH
        Hx *= invH
        Hy *= invH
        Hz *= invH
        invA = 1.0 / np.sqrt(Ax * Ax + Ay * Ay + Az * Az)
        Ax *= invA
        Ay *= invA
        Az *= invA

        Mx = Ay * Hz - Az * Hy
        My = Az * Hx - Ax * Hz
        Mz = Ax * Hy - Ay * Hx

        if R is not None:
            if len(R) == 9:
                R[0] = Hx
                R[1] = Hy
                R[2] = Hz
                R[3] = Mx
                R[4] = My
                R[5] = Mz
                R[6] = Ax
                R[7] = Ay
                R[8] = Az
            elif len(R) == 16:
                R[0] = Hx
                R[1] = Hy
                R[2] = Hz
                R[3] = 0
                R[4] = Mx
                R[5] = My
                R[6] = Mz
                R[7] = 0
                R[8] = Ax
                R[9] = Ay
                R[10] = Az
                R[11] = 0
                R[12] = 0
                R[13] = 0
                R[14] = 0
                R[15] = 0
        if I is not None:
            invE = 1.0 / np.sqrt(Ex * Ex + Ey * Ey + Ez * Ez)
            c = (Ex * Mx + Ey * My + Ez * Mz) * invE
            s = (Ex * Ax + Ey * Ay + Ez * Az) * invE
            if len(I) == 9:
                I[0] = 1
                I[1] = 0
                I[2] = 0
                I[3] = 0
                I[4] = c
                I[5] = s
                I[6] = 0
                I[7] = -s
                I[8] = c
            elif len(I) == 16:
                I[0] = 1
                I[1] = 0
                I[2] = 0
                I[4] = 0
                I[5] = c
                I[6] = s
                I[8] = 0
                I[9] = -s
                I[10] = c
                I[3] = 0
                I[7] = 0
                I[11] = 0
                I[12] = 0
                I[13] = 0
                I[14] = 0
                I[15] = 1
        return True

    def getRotationMatrixFromVector(self, R, rotationVector):
        q1 = rotationVector[0]
        q2 = rotationVector[1]
        q3 = rotationVector[2]

        if len(rotationVector) >= 4:
            q0 = rotationVector[3]
        else:
            q0 = 1 - q1 * q1 - q2 * q2 - q3 * q3
            if q0 > 0:
                q0 = np.sqrt(q0)
            else:
                q0 = 0

        sq_q1 = 2 * q1 * q1
        sq_q2 = 2 * q2 * q2
        sq_q3 = 2 * q3 * q3

        q1_q2 = 2 * q1 * q2
        q3_q0 = 2 * q3 * q0
        q1_q3 = 2 * q1 * q3

        q2_q0 = 2 * q2 * q0
        q2_q3 = 2 * q2 * q3
        q1_q0 = 2 * q1 * q0

        if len(R) == 9:
            R[0] = 1 - sq_q2 - sq_q3
            R[1] = q1_q2 - q3_q0
            R[2] = q1_q3 + q2_q0

            R[3] = q1_q2 + q3_q0
            R[4] = 1 - sq_q1 - sq_q3
            R[5] = q2_q3 - q1_q0

            R[6] = q1_q3 - q2_q0
            R[7] = q2_q3 + q1_q0
            R[8] = 1 - sq_q1 - sq_q2
        elif len(R) == 16:
            R[0] = 1 - sq_q2 - sq_q3
            R[1] = q1_q2 - q3_q0
            R[2] = q1_q3 + q2_q0
            R[3] = 0.0
            R[4] = q1_q2 + q3_q0
            R[5] = 1 - sq_q1 - sq_q3
            R[6] = q2_q3 - q1_q0
            R[7] = 0.0
            R[8] = q1_q3 - q2_q0
            R[9] = q2_q3 + q1_q0
            R[10] = 1 - sq_q1 - sq_q2
            R[11] = 0.0
            R[12] = R[13] = R[14] = 0.0
            R[15] = 1.0



    def getOrientation(self, R, values):
        if len(R) == 9:
            values[0] = math.atan2(R[1], R[4])
            values[1] = math.asin(-R[7])
            values[2] = math.atan2(-R[6], R[8])
        else:
            values[0] = math.atan2(R[1], R[5])
            values[1] = math.asin(-R[9])
            values[2] = math.atan2(-R[8], R[10])

        return values

    def onSensorChanged(self, sensorType, values, timestamp):
        if sensorType == TYPE_ACCELEROMETER:
            self.accel = list(values)
            self.calculateAccMagOrientation()
        elif sensorType == TYPE_GYROSCOPE:
            self.gyroFunction(values, timestamp)
        elif sensorType == TYPE_MAGNETIC_FIELD:
            self.magnet = list(values)
#            self.magnet[0] = 0.01
#            self.magnet[1] = 0.01
#            self.magnet[2] = 0.01

    def calculateAccMagOrientation(self):
#        print "accel:[%.4f, %.4f, %.4f]"%(self.accel[0], self.accel[1], self.accel[2])
        if self.getRotationMatrix(self.rotationMatrix, None, self.accel, self.magnet):
            self.getOrientation(self.rotationMatrix, self.accMagOrientation)
            self.accMagOrientationValid = 1
            #print "accMagOrientation:[%.4f, %.4f, %.4f]"%(self.accMagOrientation[0] * 180 / math.pi, self.accMagOrientation[1] * 180 / math.pi, self.accMagOrientation[2] * 180 / math.pi)
        else:
            self.accMagOrientationValid = 0

    def getRotationVectorFromGyro(self, gyroValues, deltaRotationVector, timeFactor):
        normValues = [0.0, 0.0, 0.0]
        omegaMagnitude = np.sqrt(gyroValues[0] * gyroValues[0] +
    		gyroValues[1] * gyroValues[1] +
    		gyroValues[2] * gyroValues[2])

        if omegaMagnitude > self.EPSILON:
            normValues[0] = gyroValues[0] / omegaMagnitude
            normValues[1] = gyroValues[1] / omegaMagnitude
            normValues[2] = gyroValues[2] / omegaMagnitude

        thetaOverTwo = omegaMagnitude * timeFactor
        sinThetaOverTwo = math.sin(thetaOverTwo)
        cosThetaOverTwo = math.cos(thetaOverTwo)

        deltaRotationVector[0] = sinThetaOverTwo * normValues[0]
        deltaRotationVector[1] = sinThetaOverTwo * normValues[1]
        deltaRotationVector[2] = sinThetaOverTwo * normValues[2]
        deltaRotationVector[3] = cosThetaOverTwo

    def getRotationMatrixFromOrientation(self, o):
        xM = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        yM = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        zM = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        sinX = math.sin(o[1])
        cosX = math.cos(o[1])
        sinY = math.sin(o[2])
        cosY = math.cos(o[2])
        sinZ = math.sin(o[0])
        cosZ = math.cos(o[0])

        xM[0] = 1.0
        xM[1] = 0.0
        xM[2] = 0.0
        xM[3] = 0.0
        xM[4] = cosX
        xM[5] = sinX
        xM[6] = 0.0
        xM[7] = -sinX
        xM[8] = cosX

        yM[0] = cosY
        yM[1] = 0.0
        yM[2] = sinY
        yM[3] = 0.0
        yM[4] = 1.0
        yM[5] = 0.0
        yM[6] = -sinY
        yM[7] = 0.0
        yM[8] = cosY

        zM[0] = cosZ
        zM[1] = sinZ
        zM[2] = 0.0
        zM[3] = -sinZ
        zM[4] = cosZ
        zM[5] = 0.0
        zM[6] = 0.0
        zM[7] = 0.0
        zM[8] = 1.0

        resultMatrix = self.matrixMultiplication(xM, yM)
        resultMatrix = self.matrixMultiplication(zM, resultMatrix)
        return resultMatrix

    def matrixMultiplication(self, A, B):
        result = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6]
        result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7]
        result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8]

        result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6]
        result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7]
        result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8]

        result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6]
        result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7]
        result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8]

        return result

    def matrixMultiplication2(self, A, B):
        result = [0.0, 0.0, 0.0]

        result[0] = A[0] * B[0] + A[1] * B[1] + A[2] * B[2]
        result[1] = A[0] * B[3] + A[1] * B[4] + A[2] * B[5]
        result[2] = A[0] * B[6] + A[1] * B[7] + A[2] * B[8]

        return result

    def gyroFunction(self, values, ltimeStamp):
        if self.accMagOrientationValid == 0:
            return

        if self.initState:
            initMatrix = self.getRotationMatrixFromOrientation(self.accMagOrientation)
            test = [0.0, 0.0, 0.0]
            self.getOrientation(initMatrix, test)
            self.gyroMatrix = self.matrixMultiplication(self.gyroMatrix, initMatrix)
            self.initState = False

        deltaVector = [0.0, 0.0, 0.0, 0.0]
        if self.timestamp != 0:
#            print "%.4f"%self.timestamp
            dT = (ltimeStamp - self.timestamp)
#            print "%.4f"%dT
            self.gyro = list(values)
            self.getRotationVectorFromGyro(self.gyro, deltaVector, dT / 2.0)

#        print deltaVector
        self.timestamp = ltimeStamp

        deltaMatrix = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.getRotationMatrixFromVector(deltaMatrix, deltaVector)
#        print deltaMatrix
        self.gyroMatrix = self.matrixMultiplication(self.gyroMatrix, deltaMatrix)
#        print self.gyroMatrix
        self.getOrientation(self.gyroMatrix, self.gyroOrientation)
        print "gyroOrientation:[%.4f, %.4f, %.4f]"%(self.gyroOrientation[0] * 180 / math.pi, self.gyroOrientation[1] * 180 / math.pi, self.gyroOrientation[2] * 180 / math.pi)

    def onPeriodicTimer(self):
        trueacceleration = [0.0, 0.0, 0.0]
        oneMinusCoeff = 1.0 - self.FILTER_COEFFICIENT

        #azimuth
        if self.gyroOrientation[0] < -0.5 * math.pi and self.accMagOrientation[0] > 0.0:
            self.fusedOrientation[0] = (self.FILTER_COEFFICIENT * (self.gyroOrientation[0] + 2.0 * math.pi) + oneMinusCoeff * self.accMagOrientation[0])
            if self.fusedOrientation[0] > math.pi:
                self.fusedOrientation[0] -= 2.0 * math.pi
        elif self.accMagOrientation[0] < -0.5 * math.pi and self.gyroOrientation[0] > 0.0:
            self.fusedOrientation[0] = self.FILTER_COEFFICIENT * self.gyroOrientation[0] + oneMinusCoeff * self.accMagOrientation[0] + 2.0 * math.pi
            if self.fusedOrientation[0] > math.pi:
                self.fusedOrientation[0] -= 2.0 * math.pi
        else:
            self.fusedOrientation[0] = self.FILTER_COEFFICIENT * self.gyroOrientation[0] + oneMinusCoeff * self.accMagOrientation[0]

        #pitch
        if self.gyroOrientation[1] < -0.5 * math.pi and self.accMagOrientation[1] > 0.0:
            self.fusedOrientation[1] = (self.FILTER_COEFFICIENT * (self.gyroOrientation[1] + 2.0 * math.pi) + oneMinusCoeff * self.accMagOrientation[1])
            if self.fusedOrientation[1] > math.pi:
                self.fusedOrientation[1] -= 2.0 * math.pi
        elif self.accMagOrientation[1] < -0.5 * math.pi and self.gyroOrientation[1] > 0.0:
            self.fusedOrientation[1] = self.FILTER_COEFFICIENT * self.gyroOrientation[1] + oneMinusCoeff * self.accMagOrientation[1] + 2.0 * math.pi
            if self.fusedOrientation[1] > math.pi:
                self.fusedOrientation[1] -= 2.0 * math.pi
        else:
            self.fusedOrientation[1] = self.FILTER_COEFFICIENT * self.gyroOrientation[1] + oneMinusCoeff * self.accMagOrientation[1]

        #roll
        if self.gyroOrientation[2] < -0.5 * math.pi and self.accMagOrientation[2] > 0.0:
            self.fusedOrientation[2] = (self.FILTER_COEFFICIENT * (self.gyroOrientation[2] + 2.0 * math.pi) + oneMinusCoeff * self.accMagOrientation[2])
            if self.fusedOrientation[2] > math.pi:
                self.fusedOrientation[2] -= 2.0 * math.pi
        elif self.accMagOrientation[2] < -0.5 * math.pi and self.gyroOrientation[2] > 0.0:
            self.fusedOrientation[2] = self.FILTER_COEFFICIENT * self.gyroOrientation[2] + oneMinusCoeff * self.accMagOrientation[2] + 2.0 * math.pi
            if self.fusedOrientation[2] > math.pi:
                self.fusedOrientation[2] -= 2.0 * math.pi
        else:
            self.fusedOrientation[2] = self.FILTER_COEFFICIENT * self.gyroOrientation[2] + oneMinusCoeff * self.accMagOrientation[2]

        self.gyroMatrix = self.getRotationMatrixFromOrientation(self.fusedOrientation)
        self.gyroOrientation = list(self.fusedOrientation)

        trueacceleration = self.matrixMultiplication2(self.accel, self.gyroMatrix)

        '''
        trueacceleration[0] = self.accel[0] * (math.cos(self.fusedOrientation[2]) * math.cos(self.fusedOrientation[0]) + \
                                math.sin(self.fusedOrientation[2]) * math.sin(self.fusedOrientation[1]) * math.sin(self.fusedOrientation[0])) + \
                                self.accel[1] * (math.cos(self.fusedOrientation[1]) * math.sin(self.fusedOrientation[0])) + \
                                self.accel[2]* (-math.sin(self.fusedOrientation[2]) * math.cos(self.fusedOrientation[0]) + math.cos(self.fusedOrientation[2]) * math.sin(self.fusedOrientation[1]) * math.sin(self.fusedOrientation[0]))

        trueacceleration[1] = self.accel[0] * (-math.cos(self.fusedOrientation[2]) * math.sin(self.fusedOrientation[0]) + math.sin(self.fusedOrientation[2]) * math.sin(self.fusedOrientation[1]) * math.cos(self.fusedOrientation[0])) + \
                                self.accel[1]*(math.cos(self.fusedOrientation[1]) * math.cos(self.fusedOrientation[0])) + \
                                self.accel[2]*(math.sin(self.fusedOrientation[2]) * math.sin(self.fusedOrientation[0])+ math.cos(self.fusedOrientation[2]) * math.sin(self.fusedOrientation[1]) * math.cos(self.fusedOrientation[0]))

        trueacceleration[2] = self.accel[0] * (math.sin(self.fusedOrientation[2]) * math.cos(self.fusedOrientation[1])) + \
                            self.accel[1] * (-math.sin(self.fusedOrientation[1])) + \
                            self.accel[2] * (math.cos(self.fusedOrientation[2]) * math.cos(self.fusedOrientation[1]))
        '''
        print "true accel: %.6f, %.6f, %.6f"%(trueacceleration[0], trueacceleration[1], trueacceleration[2])




class btGattThread(threading.Thread):
        #little endian
        accl1 = None
        accl2 = None
        accl1_h = None
        accl1_ema = None
        alpha = 0.8
        trigger_threshold = 6.0
        trigger_state = "waitfor_threshold"
        trigger_data = None
        trigger_index = 0

        accx = None

        fusion = None

        def bytes_merge(self, barray, l):
                abytes = bytearray([barray[i] for i in xrange(0, l)])
                s = 'h'
                if l == 4:
                    s = 'i'
                return struct.unpack('<' + s,abytes)[0]

        def parse_data(self, frame):
                global accDataLock

                timestamp=self.bytes_merge(frame[0:4], 4)
                framenum=self.bytes_merge(frame[4:8], 4)
                accldata1_1=self.bytes_merge(frame[8:10], 2)
                accldata1_2=self.bytes_merge(frame[10:12], 2)
                accldata1_3=self.bytes_merge(frame[12:14], 2)
                accldata2_1=self.bytes_merge(frame[14:16], 2)
                accldata2_2=self.bytes_merge(frame[16:18], 2)
                accldata2_3=self.bytes_merge(frame[18:20], 2)
                gyrodata1=self.bytes_merge(frame[20:22], 2)
                gyrodata2=self.bytes_merge(frame[22:24], 2)
                gyrodata3=self.bytes_merge(frame[24:26], 2)
                mdata1=self.bytes_merge(frame[26:28], 2)
                mdata2=self.bytes_merge(frame[28:30], 2)
                mdata3=self.bytes_merge(frame[30:32], 2)
#                print "."

#                print "MAG: [%.4f, %.4f, %.4f]"%(mdata1, mdata2, mdata3)
                self.fusion.onSensorChanged(TYPE_MAGNETIC_FIELD, [float(mdata1), float(mdata2), float(mdata3)], time.time())

                accl1_1 = float(accldata1_1) * 4 * 9.81 / float(2048.00)
                accl1_2 = float(accldata1_2) * 4 * 9.81 / float(2048.00)
                accl1_3 = float(accldata1_3) * 4 * 9.81 / float(2048.00)
#                print "ACC: [%.4f, %.4f, %.4f]"%(accl1_1, accl1_2, accl1_3)
                self.fusion.onSensorChanged(TYPE_ACCELEROMETER, [accl1_1, accl1_2, accl1_3], time.time())

                gyrodata1 = float(gyrodata1) * 2000.0 * math.pi / float(32767.0 * 180.0)
                gyrodata2 = float(gyrodata2) * 2000.0 * math.pi / float(32767.0 * 180.0)
                gyrodata3 = float(gyrodata3) * 2000.0 * math.pi / float(32767.0 * 180.0)
#                print "GYRO: [%.4f, %.4f, %.4f]"%(gyrodata1, gyrodata2, gyrodata3)
                self.fusion.onSensorChanged(TYPE_GYROSCOPE, [gyrodata1, gyrodata2, gyrodata3], time.time())

                return

                self.accl1.append(accl1_1, accl1_2, accl1_3)
                print "ACC1:[%.4f, %.4f, %.4f]"%(self.accl1[-1][0], self.accl1[-1][1], self.accl1[-1][2])

                hk = np.square(self.accl1[-1][0] - self.accl1[-2][0]) + np.square(self.accl1[-1][1] - self.accl1[-2][1]) + np.square(self.accl1[-1][2] - self.accl1[-2][2])
                hk = np.sqrt(hk)
                self.accl1_h.append(hk)
                if len(self.accl1_h) > accBufLen:
                    self.accl1_h.pop(0)

                #cal ema
                ema = self.alpha * hk + (1 - self.alpha) * self.accl1_ema[-1][0]
                f = 'e'
                if float(ema) > float(self.accl1_ema[-1][0]):
                    f = 'r'
                elif float(ema) < float(self.accl1_ema[-1][0]):
                    f = 'f'
                print "f:%c"%f

                accDataLock.acquire()
                self.accl1_ema.append((ema, f))
                if len(self.accl1_ema) > accBufLen:
                    self.accl1_ema.pop(0)

                if self.trigger_state == "waitfor_threshold":
                    if int(ema) > int(self.trigger_threshold):
                        print "triggered"
                        l = -1
                        ln = 0
                        while self.accl1_ema[l][1] != 'f':
                            l -= 1
                            ln += 1
                        if ln != 0:
                            print "waitfor falling:%d"%ln
                            self.trigger_state = "waitfor_falling_down_threshold"
                            for i in self.accl1[(-1-ln):-1]:
                                self.trigger_data.append(i)

                elif self.trigger_state == "waitfor_falling_down_threshold":
                    if self.accl1_ema[-1][1] == 'f' and int(ema) < int(self.trigger_threshold):
                        print "wait for spike"
                        self.trigger_state = "waitfor_spike"
                    self.trigger_data.append(self.accl1[-1])

                elif self.trigger_state == "waitfor_spike":
                    if self.accl1_ema[-1][1] != 'f':
                        self.trigger_state = "waitfor_threshold"
                        print "gesture %d detected:"%(self.trigger_index)
                        print self.trigger_data

                        '''
                        path = "data" + os.sep + "gesture_%d"%(self.trigger_index) + ".txt"
                        f = open(path, "w")

                        abuf = []
                        for i in self.trigger_data:
                            abuf.append("%.3f %.3f %.3f"%(i[0], i[1], i[2]))

                        f.write('\n'.join(abuf))
                        f.close()
                        '''

                        #need to do sth. there to generate the samples
                        data = np.array(self.trigger_data)
                        acc_data_x = scale(data[:, 0])
                        x = np.linspace(0, len(acc_data_x), len(acc_data_x))
                        f_accx = interp1d(x, acc_data_x)
                        x_new = np.linspace(0, len(acc_data_x), samples_num)
                        self.accx_stretch = f_accx(x_new)

                        self.trigger_index += 1
                        self.trigger_data = []
                    else:
                        self.trigger_data.append(self.accl1[-1])

                accDataLock.release()

#                print "frame:%d"%framenum
#                print "accl1:"
#                print self.accl1[-1]
#                print "accl2:"
#                print self.accl2[-1]

        def __init__(self):
                threading.Thread.__init__(self)

                self.accl1 = [(0.00,0.00,0.00)]
                self.accl1_h = [0.00]
                self.accl1_ema = [(0.00, 0.00)]

                self.accl2 = [(0.00,0.00,0.00)]

                self.trigger_data = []
                self.accx_stretch = np.arange(0, samples_num, samples_num)

                self.gatt=pexpect.spawn("gatttool -t random -b " + BT_MAC_ADDRESS + " -I")
                #self.gatt.logfile=sys.stdout
                self.gatt.sendline("connect")
                self.gatt.expect("Connection successful")
                print "connection done"
                self.conn = True

                self.fusion = sensorFusion()

        def abort(self):
                self.conn = False

        def run(self):
                global DATA_LENGTH
                global DATA_PACK
                global STATE
                global frame

                while self.conn is True:
                        self.gatt.sendline("char-read-uuid " + UUID_DATA_TX)
                        self.gatt.expect("handle: 0x0010 	 value: ")
                        self.gatt.expect(" \r\n")
                        self.gatt.sendline("char-read-uuid " + UUID_DATA_RX)
                        self.gatt.expect("handle: 0x000d 	 value: ")
                        self.gatt.expect(" \r\n")
                        # set device id
                        self.gatt.sendline("char-write-req 10 3A01030004000D0A")
                        # set transfer mode
                        self.gatt.sendline("char-write-req 10 3A0105010108000D0A")
                        # set internval
                        self.gatt.sendline("char-write-req 10 3A010703010C000D0A")
                        # set notify
                        self.gatt.sendline("char-write-cmd 0e 0100")
                        # set datastart
                        self.gatt.sendline("char-write-req 10 3A010A000B000D0A")

                        idx = 0
                        while self.conn is True:
                                self.gatt.expect("Notification handle = 0x000d value:")
                                resp = (self.gatt.before).decode('UTF-8').replace(" ", "")
                                if DATA_LENGTH == 0 and STATE != 0:
                                        STATE = 0
                                        self.parse_data(frame)
                                        frame=[]
                                if STATE == 0:
                                        if cmp(resp[0:6], '3a0109') == 0:
                                                STATE = 1
                                                DATA_PACK=1
                                                DATA_LENGTH=int(resp[6:8], 16)
                                                idx = 0
                                                while DATA_LENGTH > 0:
                                                        a=8+idx*2
                                                        b=10+idx*2
                                                        try:
                                                                frame.append(int(resp[a:b], 16))
                                                        except ValueError:
                                                                break
                                                        idx += 1
                                                        DATA_LENGTH-=1
                                elif STATE == 1:
                                        idx = 0
                                        while DATA_LENGTH > 0:
                                                a=idx*2
                                                b=2+idx*2
                                                try:
                                                        frame.append(int(resp[a:b], 16))
                                                except ValueError:
                                                        break
                                                idx += 1
                                                DATA_LENGTH-=1

samples_num = 15

axcolor = 'lightgoldenrodyellow'
fig = plt.figure(1)

gattClient = btGattThread()
gattClient.start()

ax1 = fig.add_subplot(2, 1, 1, xlim=(0, accBufLen), ylim=(0, 20))
ax1_line = [0.0 for i in xrange(0, accBufLen)]

ax2 = fig.add_subplot(2, 1, 2, xlim=(0, samples_num), ylim=(-16, 16))
ax2_line = [0.0 for i in xrange(0, samples_num)]

#actually below is ema lines
acc_line, = ax1.plot(ax1_line)
# accx line
accx_line, = ax2.plot(ax2_line)

def init():
    global acc_line
    global ax1_line

    acc_line.set_ydata(ax1_line)
    accx_line.set_ydata(ax2_line)

    return (acc_line, accx_line)

def update(val):
    global acc_line
    global ax1_line
    global gattClient
    global accDataLock

    accDataLock.acquire()
    for i in xrange(0, len(gattClient.accl1_ema)):
        ax1_line.append(gattClient.accl1_ema[i][0])
        ax1_line.pop(0)

    ax2_line = gattClient.accx_stretch.copy()
    accDataLock.release()

    acc_line.set_ydata(ax1_line)
    accx_line.set_ydata(ax2_line)

    gattClient.fusion.onPeriodicTimer()

    return (acc_line, accx_line)

ani = animation.FuncAnimation(fig, update, init_func=init, interval=1*500)

plt.show()
gattClient.abort()
gattClient.join()



